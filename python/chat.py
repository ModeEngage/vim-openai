import json
import requests
import vim

messages = []
for message in vim.vars['openai_chatgpt_sessions'][str(vim.current.buffer.number)]:
    messages.append({'role': message['role'].decode(), 'content': message['content'].decode()})

model_response = requests.post(
    "https://api.openai.com/v1/chat/completions",
    headers={
        "Content-Type": "application/json",
        "Authorization": f"Bearer {vim.vars['openai_bearer_token'].decode('utf-8')}"
    },
    json={
        "model": "gpt-3.5-turbo",
        "messages": messages
    }
)

if model_response.status_code >= 200 and model_response.status_code < 300:
    new_message = vim.Dictionary()
    new_message['role'] = 'assistant'.encode()
    new_message['content'] = model_response.json()['choices'][0]['message']['content'].encode()
    vim.vars['openai_chatgpt_sessions'][str(vim.current.buffer.number)].extend([new_message])
else:
    vim.current.buffer.append(f"Error: API responded with status code {model_response.status_code}")

vim.current.buffer[:] = None
for render_message in vim.vars['openai_chatgpt_sessions'][str(vim.current.buffer.number)]:
    vim.current.buffer.append(f"{render_message['role'].decode()}:")
    vim.current.buffer.append(render_message['content'].decode().strip().split('\n'))
    vim.current.buffer.append('')
