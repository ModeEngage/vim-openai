import json
import requests
import vim

input = ""
buffer_range = vim.current.buffer.range(int(sys.argv[0]), int(sys.argv[1]))
for line in buffer_range:
    input = f'{input}\n{line}'

model_response = requests.post(
    "https://api.openai.com/v1/edits",
    headers={
        "Content-Type": "application/json",
        "Authorization": f"Bearer {vim.vars['openai_bearer_token'].decode('utf-8')}"
    },
    json={
        "model": "code-davinci-edit-001",
        "input": input,
        "instruction": sys.argv[2].strip()
    }
)

if model_response.status_code >= 200 and model_response.status_code < 300:
    buffer_range[:] = None 
    buffer_range.append(model_response.json()['choices'][0]['text'].strip().split('\n'))
else:
    vim.current.buffer.append(f"Error: API responded with status code {model_response.status_code}")
