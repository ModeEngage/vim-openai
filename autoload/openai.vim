vim9script

const g:openai_python_directory = $"{fnamemodify(resolve(expand('<sfile>:p')), ':h')}/../python"
final g:openai_chatgpt_sessions = {}

def NewBuffer(): number
  var bufnr = bufadd("")
  bufload(bufnr)
  return bufnr
enddef

def NewTabFromBuffer(bufnr: number)
  execute $'tab sb {bufnr}'
enddef

def NewChatGptSession(bufnr: number)
  g:openai_chatgpt_sessions[string(bufnr)] = []
enddef

def ChatGptSessionExists(bufnr: number): bool
  return g:openai_chatgpt_sessions->has_key(string(bufnr))
enddef

def AddUserContent(bufnr: number, content: string)
  g:openai_chatgpt_sessions[string(bufnr)]->add({
    role: "user",
    content: content
  })
enddef

def SendCurrentBuffer()
  execute $'py3file {g:openai_python_directory}/chat.py'
enddef

def QueryFromArgs(prompt: string, queryArgs: list<string>): string
  var query = ""
  for arg in queryArgs
    if query == ""
      query = arg
    else
      query = $'{query} {arg} '
    endif
  endfor
  if query == ""
    query = input($'{prompt}: ')
  endif
  return query
enddef

# Opens a dedicated chat session in a new buffer and displays it in a tab
export def OpenAiChatTab(...queryArgs: list<string>)
  var query = QueryFromArgs('Chat prompt', queryArgs)
  var bufnr = NewBuffer()
  NewTabFromBuffer(bufnr)
  NewChatGptSession(bufnr)
  AddUserContent(bufnr, query)
  SendCurrentBuffer()
enddef

# Adds a reply to an existing chat session
export def OpenAiChatReply(...queryArgs: list<string>)
  var bufnr = bufnr("%")
  if ChatGptSessionExists(bufnr)
    var query = QueryFromArgs('Chat reply', queryArgs)
    AddUserContent(bufnr("%"), query)
    SendCurrentBuffer()
  else
    throw 'Buffer is not a valid ChatGPT session'
  endif
enddef

# Submits an edit request for the selected buffer lines;
export def OpenAiEditSelection(start: number, end: number, ...queryArgs: list<string>)
  var instruction = QueryFromArgs('Instructions', queryArgs)
  execute $'python3 sys.argv = ["{start}", "{end}", "{instruction}"]'
  execute $'py3file {g:openai_python_directory}/edit.py'
enddef
