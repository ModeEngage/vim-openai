## Usage

Set `g:openai_bearer_token` to the appropriate bearer token somewhere in your vim configuration.

## License

MIT
