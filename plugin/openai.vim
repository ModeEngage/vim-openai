if exists('g:loaded_vim_openai')
  finish
endif

command! -nargs=? ChatTab call openai#OpenAiChatTab(<f-args>)
command! -nargs=? ChatReply call openai#OpenAiChatReply(<f-args>)
command! -range=% -nargs=? EditSelection call openai#OpenAiEditSelection(<line1>, <line2>, <f-args>)

let g:loaded_vim_openai = 1
